import pandas as pd
import numpy as np
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis, QuadraticDiscriminantAnalysis
from sklearn.linear_model import LogisticRegression

# initialize classifiers
lg_optimizer = 'saga'
lg = LogisticRegression(solver=lg_optimizer, multi_class='auto')
lda = LinearDiscriminantAnalysis()
qda = QuadraticDiscriminantAnalysis()

# load train a test data
train_data = pd.read_csv('./vowel.train.txt')
test_data = pd.read_csv('./vowel.test.txt')
# separante features from classes
y_train = train_data['y']
y_test = test_data['y']
# delete row counts from data set
X_train = train_data.drop(['row.names', 'y'], axis=1)
X_test = test_data.drop(['row.names', 'y'], axis=1)

# train classifiers
lg.fit(X_train, y_train)
lda.fit(X_train, y_train)
qda.fit(X_train, y_train)

# predict on test data
lg_pred = lg.predict(X_test)
lda_pred = lda.predict(X_test)
qda_pred = qda.predict(X_test)

# calculate test erros
lg_error = np.average((y_test - lg_pred)**2)
lda_error = np.average((y_test - lda_pred)**2)
qda_error = np.average((y_test - qda_pred)**2)

print('\n--- TEST ERRORS --------------')
print('LOGISTIC REGRESSION: {} ({})'.format(lg_error, lg_optimizer))
print('LINEAR DISCRIMINANT ANALYSIS: {}'.format(lda_error))
print('QUADRATIC DISCRIMINANT ANALYSIS: {}'.format(qda_error))