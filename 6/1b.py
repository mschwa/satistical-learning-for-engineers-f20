import pandas as pd
import numpy as np
from sklearn.linear_model import LogisticRegression
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap

# load content of csv file into DataFrame object
data = pd.read_csv('../3/ESLmixture.csv')
X = data[['x.1', 'x.2']]
y = data['y']
# get statistics from data
x1_min = data['x.1'].min()
x1_max = data['x.1'].max()
x2_min = data['x.2'].min()
x2_max = data['x.2'].max()

# define mesh step-size and margin for mesh generation
h = 0.075
d = 0.5
# create mesh with coordinates along each axis increasing by h
x1_coords = np.arange(x1_min - d, x1_max + d, h)
x2_coords = np.arange(x2_min - d, x2_max + d, h)
X1, X2 = np.meshgrid(x1_coords, x2_coords)

# create colormap to associate colors  y-values (order is blue, orange)
cmap_mesh = ListedColormap(['#93C8D9', '#F7BC7E'])
cmap_scatter = ListedColormap(['#0000FF', '#FFA500'])

# linear regression classifier
lr = LogisticRegression()
lr.fit(X, y)
# flatten arrays with np.ravel into x1 and x2, and efficiently calculate their.
# cartesian product. This gives all the points in the mesh, which are used for
# prediction
pred = lr.predict(np.c_[X1.ravel(), X2.ravel()])
# put predictions in same shape as meshgrid
pred = pred.reshape(X1.shape)

fig = plt.figure()
# decision boundary is where ratio of probabilities is 1, so log
# of ratio is zero. For two variables, line of equation is give as
# y = (-1 / beta[2]) * (beta[0] + beta[1] * x)
coeffs = lr.coef_[0]
intercept = lr.intercept_
x2_bound = [(-1 / coeffs[1]) * (intercept + coeffs[0] * x1) for x1 in x1_coords]
plt.plot(x1_coords, x2_bound, 'k-')

# put the result into a color plot, using the classification method explained
# in section 2.3
plt.title('Logistic Regression of 0/1 Response')
plt.pcolormesh(X1, X2, pred > 0.5, edgecolor='white', cmap=cmap_mesh, shading='auto')

# Plot also the training points
plt.scatter(data['x.1'], data['x.2'], cmap=cmap_scatter,c=y, marker='o')
plt.xlim(x1_min - d, x1_max + d)
plt.ylim(x2_min - d, x2_max + d)

# hide axes in plot
fig.axes[0].get_xaxis().set_visible(False)
fig.axes[0].get_yaxis().set_visible(False)

# display plots
plt.show()
