import math
import pandas as pd
import numpy as np
from sklearn.linear_model import LogisticRegression
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap

# load content of csv file into DataFrame object
data = pd.read_csv('../3/ESLmixture.csv')
X = data[['x.1', 'x.2']]
y = data['y']
# add column of 1s to data
ones = np.ones((len(y), 1))
X.insert(0, 'x.0', ones)
n_rows, n_cols = X.shape
# convert X and y to np arrays
X = X.to_numpy()
y = y.to_numpy()

# define sigmoid function
sigmoid = lambda x: 1 / (1 + math.exp(-x))

def prob_matrices(beta_old):
    # initialize vector of data point probabilites
    # and diagonal of W matrix
    p = np.zeros((n_rows,))
    diag = np.zeros((n_rows,))
    # iterate over rows of dataset:
    for i, x in enumerate(X):
        # calculate pi for each data point
        dot = np.dot(beta_old, x)
        pi = sigmoid(dot)
        # update corresponding p and diag entries
        p[i] = pi
        diag[i] = pi * (1 - pi)
    # construct W matrix from diagonal
    return p, np.diag(diag)    


def train():
    # initialize beta
    beta_old = np.zeros((n_cols,))
    # update beta until it converges
    while (True):
        p, W = prob_matrices(beta_old)
        # calculate z as defined in (4.27)
        z = X @ beta_old + np.linalg.inv(W) @ (y - p)
        # precalculate X transpose matrix
        Xt = np.transpose(X)
        # update beta using (4.26)
        beta = np.linalg.inv(Xt @ W @ X) @ Xt @ W @ z
        # check convergence and update beta for next iteration
        if (np.linalg.norm(beta) - np.linalg.norm(beta_old) < 0.000001): break
        beta_old = beta

    return beta

def predict(data_pts, beta):
    # get number of coordinate pairs and initialize prediction array
    r, c = data_pts.shape
    preds = np.zeros((r,))
    # populate prediction array
    for i, x in enumerate(data_pts):
        preds[i] = beta[0] + np.dot(beta[1:], x)
    
    return preds


def main():
    beta = train()

    # get statistics from original data
    x1_min = data['x.1'].min()
    x1_max = data['x.1'].max()
    x2_min = data['x.2'].min()
    x2_max = data['x.2'].max()
    # define mesh step-size and margin for mesh generation
    h = 0.075
    d = 0.5
    # create mesh with coordinates along each axis increasing by h
    x1_coords = np.arange(x1_min - d, x1_max + d, h)
    x2_coords = np.arange(x2_min - d, x2_max + d, h)
    X1, X2 = np.meshgrid(x1_coords, x2_coords)

    # create colormap to associate colors  y-values (order is blue, orange)
    cmap_mesh = ListedColormap(['#93C8D9', '#F7BC7E'])
    cmap_scatter = ListedColormap(['#0000FF', '#FFA500'])

    # flatten arrays with np.ravel into x1 and x2, and efficiently calculate their.
    # cartesian product. This gives all the points in the mesh, which are used for
    # prediction
    mesh_points = np.c_[X1.ravel(), X2.ravel()]
    pred = predict(mesh_points, beta)
    # put predictions in same shape as meshgrid
    pred = pred.reshape(X1.shape)

    fig = plt.figure()
    # decision boundary is where ratio of probabilities is 1, so log
    # of ratio is zero. For two variables, line of equation is give as
    # y = (-1 / beta[2]) * (beta[0] + beta[1] * x)
    x2_bound = [(-1 / beta[2]) * (beta[0] + beta[1] * x1) for x1 in x1_coords]
    plt.plot(x1_coords, x2_bound, 'k-')

    # put the result into a color plot, using the classification method explained
    # in section 2.3
    plt.title('Logistic Regression of 0/1 Response')
    plt.pcolormesh(X1, X2, pred > 0, edgecolor='white', cmap=cmap_mesh, shading='auto')

    # Plot also the training points
    plt.scatter(data['x.1'], data['x.2'], cmap=cmap_scatter,c=y, marker='o')
    plt.xlim(x1_min - d, x1_max + d)
    plt.ylim(x2_min - d, x2_max + d)

    # hide axes in plot
    fig.axes[0].get_xaxis().set_visible(False)
    fig.axes[0].get_yaxis().set_visible(False)

    # # display plots
    plt.show()

if __name__ == '__main__':
    main()
