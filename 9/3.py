import numpy as np


def policy_evaluation(dim, terminal_states):
    # initialize value grid
    v = np.zeros((dim, dim))
    # initialize convergence threshold
    threshold = 1 / 10e6

    # run policy evaluation
    while (True):
        # reset accuracy after evaluation pass on board
        delta = 0

        # update value estimates
        for row in range(dim):
            for col in range(dim):
                # if state is terminal, there's no action to take.
                if ((row, col) in terminal_states): continue
                # record old state value and initialize its new value
                v_old = v[row][col]
                v_new = 0
                # calculate new value using most up-to-date values available
                # (for faster convergence)
                for (i, j) in [(-1, 0), (1, 0), (0, -1), (0, 1)]:
                    # min and max handle edge cases off the grid
                    adj_row = min(max(row + i, 0), dim - 1)
                    adj_col = min(max(col + j, 0), dim - 1)
                    # account for value of adjacent state
                    v_new += 0.25 * (-1 + v[adj_row][adj_col])

                # update this pass' grid and delta
                v[row][col] = v_new
                delta = max(delta, abs(v_new - v_old))

        # stop when algorithm converges
        if (delta <= threshold): break

    return v


if __name__ == '__main__':
    v = policy_evaluation(4, [(0, 0), (3, 3)])
    print(v)
