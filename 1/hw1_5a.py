# code adapted from https://www.python-course.eu/matplotlib_contour_plot.php

import numpy as np
import matplotlib.pyplot as plt

def draw_contour(points_range, fill = True):
    """
    Parameters
    ----------
    points_range: tuple
        Arguments for linspace to generate x- and y- coordinates of points
        to be plotted

    Returns
    ----------
    Axes
        Reference to plot created

    """
    # define minimum and maximum values of point coordinates in
    # each direction. Define the number of points to be generated
    start, end, n = points_range

    # get evenly-spaced x- and y-coordinates of input (x, y) to
    # function z(x,y)
    x_vals = np.linspace(start, end, n)
    y_vals = np.linspace(start, end, n)

    # 2-D case: considering x_vals as a (1 x n) row-vector and y_vals
    # as a (n X 1) column-vector, create (n x n) matrix X by stacking
    # x_vals n times, and (n x n) matrix Y by concatenating y_vals n
    # times. Then pairing Xij and Yij, for all 0 <= i, j < n, gives 
    # the x and y coordinates of all points that could be formed by
    # combining the x-coordinates in x-vals with the y-coordinates
    # in y_vals
    X, Y = np.meshgrid(x_vals, y_vals)

    # evaulate z(x, y) for all poscreenints generated
    Z = 100*(Y - X**2)**2 + (1 - X)**2

    # create new figure with specified width and height in inches
    fig = plt.figure(figsize=(6,5))
    # set padding between plot and figure edges in fractions of
    # figure width and height
    left, bottom, width, height = 0.1, 0.1, 0.8, 0.8
    ax = fig.add_axes([left, bottom, width, height]) 

    # draw filled contours for function based on its input and output
    # and add a colorbar to ir
    if (fill):
        cp = plt.contourf(X, Y, Z, 10)
        plt.colorbar(cp)
    else:
        plt.contour(X, Y, Z, 10)

    # name graph and axes
    ax.set_title('Contour Plot')
    ax.set_xlabel('x')
    ax.set_ylabel('y')

    # allow further plotting
    return ax

if __name__ == '__main__':
    draw_contour((-0.5, 5, 1000))
    plt.show()