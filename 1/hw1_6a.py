# code adapted from https://towardsdatascience.com/implement-gradient-descent-in-python-9b93ed7108d1
import numpy as np
import matplotlib.pyplot as plt

from hw1_5a import draw_contour

# generate initial point
x = np.random.rand(2, 1)

# set learning rate, termination precision and number of maximum iterations
rate = 0.00195
precision = 0.000001 
max_iters = 10000

# original f(x, y)
def f(x):
    return (100 * (x[1] - x[0]**2)**2 + (1 - x[0])**2)[0]


# given x in 2-D Eucliden space, calculate gradient of f at x
def grad(x):
    # calculate gradient elements using equations derived from homework
    gx = -400 * (x[0] * x[1] - x[0]**3) - 2 * (1 - x[0])
    gy = 200 * (x[1] - x[0]**2)
    # create gradient vector
    g = np.empty((2, 1), dtype=float)
    g[0], g[1] = gx, gy
    return g


# track the values of x, f(x) and grad(x) obtained during training
x_visited = [x]
f_x_vals = [f(x)]
grads = [grad(x)]

# define loop variables
delta_x = float('inf')
iter = 0

while delta_x > precision and iter < max_iters:
    # gradient descent
    g = grad(x)
    x_new = x - rate * g
    # distance between x_new and x
    delta_x = abs(np.linalg.norm(x_new - x))

    # update loop variables
    iter += 1
    x = x_new
    # update plot data
    x_visited.append(x)
    f_x_vals.append(f(x))
    grads.append(g)

# print results
print('Algorithm completed in {} iterations.'.format(iter, x[0][0], x[1][0]))
print('Minimum f(x) = {} at x = ({}. {})'.format(f(x), x[0][0], x[1][0]))


# draw (empty) contour and get reference to plot
ax_contour = draw_contour((-0.5, 1.5, 200), fill = False)

# # pick indices of 6 gradients to plot
# idxs = np.linspace(0, len(x_visited) - 1, 6)
# # plot gradient vectors
# for idx in idxs:
#     idx = int(idx)
#     # retrieve x and grad(x) vectors at the current index
#     x = x_visited[idx]
#     g = grads[idx]
#     # get x and y components of gradient vector
#     gx = float(g[0])
#     gy = float(g[1])
#     # for plotting: normalize gradient vector if its length is
#     # greater than one
#     grad_norm = np.linalg.norm(g)
#     if grad_norm > 1:
#         gx /= grad_norm
#         gy /= grad_norm
#     # plot gradient vector. The first two numbers are its origin,
#     # whereas the last two, its x and y dimensions (scaled down for
#     # visualization)
#     ax_contour.arrow(float(x[0]), float(x[1]), -gx * 0.5, -gy * 0.5,
#         length_includes_head=True,
#         head_width=0.05, 
#         head_length=0.025)

# plot (x, y) points on which f was evaluated during descent
x0_visited = list(map(lambda x: float(x[0]), x_visited))
x1_visited = list(map(lambda x: float(x[1]), x_visited))
ax_contour.scatter(x0_visited, x1_visited)

# create new figure with specified width and height in inches
fig = plt.figure(figsize=(6,5))
# set padding between plot and figure edges in fractions of
# figure width and height
left, bottom, width, height = 0.1, 0.1, 0.8, 0.8
ax = fig.add_axes([left, bottom, width, height]) 

# create scatter plot of f(x) vs. step
ax.scatter(list(range(iter + 1)), f_x_vals)

# name graph and axes
ax.set_title('f(x) vs. step number')
ax.set_xlabel('step number')
ax.set_ylabel('f(x)')
plt.show()
