import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import PolynomialFeatures

# set random seed
# np.random.seed(0)

# set number of samples
N = 100

# generate random X and epsilon values
X = 5 * np.random.uniform(size=N) - 2
epsilon = np.random.normal(0, 1, N)
# sort X values for plots to be generated correctly and caculate output vector
X = np.sort(X)
Y = 0.5 * X**2 + 0.5 * X + 1 + epsilon

# reshape input to 2D column array (required for fitting)
X = X.reshape(len(X), 1)
# create and fit linear regression classifier 
lin_reg = LinearRegression()
lin_reg.fit(X, Y)
# extract slope and intercept of fit line equation
slope = lin_reg.coef_
intercept = lin_reg.intercept_
print('Line equation: y = {:.2}x + {:.3}'.format(slope[0], intercept))

# declare list containing y-values of fitted polynomials over input
poly_vals = []
# perform polynomial regression for degrees p
for p in [2, 7, 50]:
    poly_features = PolynomialFeatures(degree=p, include_bias=False)
    # generate array where each entry of X is mapped to [X, x^2, ..., X^p]
    input = poly_features.fit_transform(X)
    # 1D polynomial regression is equivalent to multi-dimensional linear regression
    # with entries of feature vector consisting of powers of X
    poly_reg = LinearRegression()
    poly_reg.fit(input, Y)
    # get coefficients and intercept
    poly_coeffs = poly_reg.coef_
    poly_intercept = poly_reg.intercept_
    # evaluate polynomials over inputs
    y = poly_reg.predict(input)
    poly_vals.append(y)

# plot fitted curves
plt.scatter(X, Y, color='black')
plt.plot(X, lin_reg.predict(X), '-k', label='p = 1', linewidth=2)
plt.plot(X, poly_vals[0], '-g', label='p = 2', linewidth=2)
plt.plot(X, poly_vals[1], '-b', label='p = 7', linewidth=2)
plt.plot(X, poly_vals[2], '-r', label='p = 50', linewidth=2)

# add legend and axes' labels
plt.legend()
plt.xlabel('x')
plt.ylabel('y')

plt.show()


