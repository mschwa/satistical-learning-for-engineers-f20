import math
import pandas as pd
import numpy as np
from sklearn.neighbors import KNeighborsClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.linear_model import LogisticRegression

def error(y, y_pred):
    # for zero-one loss function, number of misclassifications equals
    # number of non-zero entries in (y - y_pred)
    v = y - y_pred
    errors = np.count_nonzero(v)
    return errors / y.shape[0]

def print_error(method, dataset, error):
    print('[{}] {} error: {:.3f}'.format(method, dataset, error))


# load train data as matrix
data_2 = np.loadtxt('./train.2.txt', delimiter=',')
data_3 = np.loadtxt('./train.3.txt', delimiter=',')
data_5 = np.loadtxt('./train.5.txt', delimiter=',')

# create target columns for each training set (np.ndarray)
y_2 = np.full((data_2.shape[0], 1), 2)
y_3 = np.full((data_3.shape[0], 1), 3)
y_5 = np.full((data_5.shape[0], 1), 5)

# load test data as matrox
test_data = np.loadtxt('./zip.test.txt')
# remove rows associated with classes we won't train for
valid_classes = { 2, 3, 5 }
for i in range(test_data.shape[0] - 1, -1, -1):
    # determine if row is valid by checking its first element
    if test_data[i][0] not in valid_classes:
        # update test with removed row
        test_data = np.delete(test_data, (i), axis=0)
# separate target from features in final test set
X_test = test_data[:, 1:]
y_test = test_data[:, 0]

# stack training points
X_train = np.vstack((data_2, data_3, data_5))
y_train = np.vstack((y_2, y_3, y_5))
# reshape column matrix as column vector (np.array)
y_train = y_train.reshape((y_train.shape[0],))

# declare classifiers
kNN_1 = KNeighborsClassifier(n_neighbors=1)
kNN_3 = KNeighborsClassifier(n_neighbors=3)
kNN_7 = KNeighborsClassifier(n_neighbors=7)
lda = LinearDiscriminantAnalysis()
lg = LogisticRegression(max_iter=5000, multi_class='auto')

# train classifiers
kNN_1.fit(X_train, y_train)
kNN_3.fit(X_train, y_train)
kNN_7.fit(X_train, y_train)
lda.fit(X_train, y_train)
lg.fit(X_train, y_train)

# get predictions on training data
pred_kNN_1_train = kNN_1.predict(X_train)
pred_kNN_3_train = kNN_3.predict(X_train)
pred_kNN_7_train = kNN_7.predict(X_train)
pred_lda_train = lda.predict(X_train)
pred_lg_train = lg.predict(X_train)

# calculate train error for each method and print it
print('\n--- TRAIN ERRORS --------------------')
print_error('1-NN', 'Train', error(y_train, pred_kNN_1_train))
print_error('3-NN', 'Train', error(y_train, pred_kNN_3_train))
print_error('7-NN', 'Train', error(y_train, pred_kNN_7_train))
print_error('LDA', 'Train', error(y_train, pred_lda_train))
print_error('LR', 'Train', error(y_train, pred_lg_train))

# get predictions on test data
pred_kNN_1_test = kNN_1.predict(X_test)
pred_kNN_3_test = kNN_3.predict(X_test)
pred_kNN_7_test = kNN_7.predict(X_test)
pred_lda_test = lda.predict(X_test)
pred_lg_test = lg.predict(X_test)

# calculate test error for each method and print it
print('\n--- TEST ERRORS --------------------')
print_error('1-NN', 'Test', error(y_test, pred_kNN_1_test))
print_error('3-NN', 'Test', error(y_test, pred_kNN_3_test))
print_error('7-NN', 'Test', error(y_test, pred_kNN_7_test))
print_error('LDA', 'Test', error(y_test, pred_lda_test))
print_error('LR', 'Test', error(y_test, pred_lg_test))



