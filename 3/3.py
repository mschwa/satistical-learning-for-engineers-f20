import pandas as pd
import numpy as np
import sklearn.neighbors as nb
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap

# load content of csv file into DataFrame object
data = pd.read_csv('ESLmixture.csv')
X = data[['x.1', 'x.2']]
y = data['y']
# get statistics from data
x1_min = data['x.1'].min()
x1_max = data['x.1'].max()
x2_min = data['x.2'].min()
x2_max = data['x.2'].max()

# define mesh step-size and margin for mesh generation
h = 0.05
d = 0.5
# create mesh with coordinates along each axis increasing by h
x1_coords = np.arange(x1_min - d, x1_max + d, h)
x2_coords = np.arange(x2_min - d, x2_max + d, h)
X1, X2 = np.meshgrid(x1_coords, x2_coords)

# create colormap to associate colors  y-values (order is blue, orange)
cmap_mesh = ListedColormap(['#93C8D9', '#F7BC7E'])
cmap_scatter = ListedColormap(['#0000FF', '#FFA500'])


def train_predict_plot(k):
    # k-neareast-neighbor classifier
    knn = nb.KNeighborsClassifier(n_neighbors=k)
    knn.fit(X, y)
    # flatten arrays with np.ravel into x1 and x2, and efficiently calculate their.
    # cartesian product. This gives all the points in the mesh, which are used for
    # prediction
    pred = knn.predict(np.c_[X1.ravel(), X2.ravel()])
    # put predictions in same shape as meshgrid
    pred = pred.reshape(X1.shape)
    # put the result into a color plot
    fig = plt.figure()
    plt.title('{}-nearest-neighbor'.format(k))
    plt.pcolormesh(X1, X2, pred, edgecolor='white', cmap=cmap_mesh)

    # Plot also the training points
    plt.scatter(data['x.1'], data['x.2'], cmap=cmap_scatter,c=y, marker='o')
    plt.xlim(x1_min - d, x1_max + d)
    plt.ylim(x2_min - d, x2_max + d)

    # hide axes in plot
    fig.axes[0].get_xaxis().set_visible(False)
    fig.axes[0].get_yaxis().set_visible(False)

    # plot class boundaries
    plt.contour(X1, X2, pred, colors='black', linewidths=0.1)


# generate plot for 15- and 1-nearest-neighbor(s)
train_predict_plot(1)
train_predict_plot(15)

# display plots
plt.show()
