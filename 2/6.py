import numpy as np
import matplotlib.pyplot as plt

# method to create pre-configured figures for plotting
def fig_factory():
    fig = plt.figure(figsize=(6,5))
    left, bottom, width, height = 0.1, 0.1, 0.8, 0.8
    ax = fig.add_axes([left, bottom, width, height]) 
    return ax

# map array of x values to value of gaussian function for those values
def gaussian_density(x):
    denom = sigma * np.sqrt(2 * np.pi)
    num = np.exp( -(x - mu)**2 / (2 * sigma**2) )
    return num / denom

# define distribution parameters along with number of bins generated in
# histogram
mu = 0
sigma = 1
n_bins = 30

# generate gaussian samples
sample_1k = np.random.normal(mu, sigma, 1000)
sample_5k = np.random.normal(mu, sigma, 5000)
sample_100k = np.random.normal(mu, sigma, 100000)

# create axes
ax1 = fig_factory()
ax1.set_title('N = 1000')
ax2 = fig_factory()
ax2.set_title('N = 5000')
ax3 = fig_factory()
ax3.set_title('N = 100000')

# create histogram with n_bins. Get the value in each bin (y-value) along with
# the edge values of the bins in the x-axis, respectively. Note that since
# density=True, each bin will display the bin's raw count divided by the total
# number of counts AND the bin width. Density is set to true so that the
# histogram's scale allows plotting the density function.
count1, edges1, _ = ax1.hist(sample_1k, n_bins, density=True)
count2, edges2, _ = ax2.hist(sample_5k, n_bins, density=True)
count3, edges3, _ = ax3.hist(sample_100k, n_bins, density=True)

# for the value of each bin, evaluate the density function and plot it
ax1.plot(edges1,
         gaussian_density(edges1),
         linewidth=2,
         color='r')
ax2.plot(edges2,
         gaussian_density(edges2),
         linewidth=2,
         color='r')
ax3.plot(edges3,
         gaussian_density(edges3),
         linewidth=2,
         color='r')

plt.show()