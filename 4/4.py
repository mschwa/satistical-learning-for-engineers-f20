import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap

# sklearn imports
import sklearn.neighbors as nb
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder, OneHotEncoder

# declare encoders
OH_encoder = OneHotEncoder(handle_unknown='ignore', sparse=False)
label_encoder = LabelEncoder()

def preprocess(loan_data):
    # separate target from features and label it
    y = loan_data['y']
    y = label_encoder.fit_transform(y)
    X = loan_data.drop(['y'], axis=1)

    # determine columns to be one-hot encoded (no hierarchy among classes)
    one_hot_cols = ['job', 'contact', 'poutcome']
    # determine columns to be labeled (hierarchy exists among classes)
    labeled_cols = ['marital', 'education', 'default', 'housing', 'loan', 'month']

    # one-hot encode appropriate columns
    X_encoded = X[one_hot_cols].copy()
    # save indexes to put the back on frame (one-hot encoding drops them)
    index = X_encoded.index
    # encode
    X_encoded = pd.DataFrame(OH_encoder.fit_transform(X_encoded))
    # reassign index
    X_encoded.index = index
    

    # label encode appropriate columns
    X_labeled = X[labeled_cols].copy()
    for col in labeled_cols:
        X_labeled[col] = label_encoder.fit_transform(X_labeled[col])

    # combine columns into processed dataset
    X_processed = X.copy()
    X_processed = X_processed.drop(one_hot_cols + labeled_cols, axis=1)
    X_processed = pd.concat([X_processed, X_encoded, X_labeled], axis=1)

    # return train and test splits
    return train_test_split(X_processed, y, train_size=0.75, test_size=0.25, random_state=0)


if __name__ == '__main__':
    # load content of csv file into DataFrame object
    loan_data = pd.read_csv('./loan_data.csv', sep=';')
    # preprocess data and get set splits
    X_train, X_test, y_train, y_test = preprocess(loan_data)

    # declare array storing data to be plotted
    ks = []
    train_errors = []
    test_errors = []

    # run k-NN for multiple k-values
    for k in range(1, 201, 10):
        print('Calculating k={}...'.format(k))
        # train model
        knn = nb.KNeighborsClassifier(n_neighbors=k)
        knn.fit(X_train, y_train)
        # predict on test data
        train_predictions = knn.predict(X_train)
        test_predictions = knn.predict(X_test)
        # compute errors
        train_error = np.average((y_train - train_predictions)**2)
        test_error = np.average((y_test - test_predictions)**2)
        
        # prepare plot
        ks.append(k)
        train_errors.append(train_error)
        test_errors.append(test_error)

    # plot
    plt.plot(ks, train_errors, '-r', ks, test_errors, '-g')
    plt.xlabel('k')
    plt.ylabel('Estimation Error')
    plt.legend(['train error', 'test_error'])
    plt.show()
    
