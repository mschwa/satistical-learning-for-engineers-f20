import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import Ridge, Lasso, LinearRegression

# set random seed
np.random.seed(0)

# set number of samples
N = 100
p = 10
solver = 'svd'    # sag or svd

# generate random X and epsilon values
x = 5 * np.random.uniform(size=N) - 2
epsilon = np.random.normal(0, 1, N)
# sort X values for plots to be generated correctly and caculate output vector
x = np.sort(x)
Y = 0.5 * x**2 + 0.5 * x + 1 + epsilon

# expand X to apply polynomial regression
X = np.zeros((N, p))
print(X.shape)
for i in range(1, p + 1, 1):
    X[:, i-1] = x**i

# create ridge and lasso models
linear = Ridge(alpha=0, solver=solver)
ridge_a = Ridge(alpha=0.00001, solver=solver)
ridge_b = Ridge(alpha=1, solver=solver)
lasso_a = Lasso(alpha=0.0000001)
lasso_b = Lasso(alpha=1)

# train ridge regression models
print('\n--------------------------------------------')
linear.fit(X, Y)
print('linear coeffs: ', linear.coef_)
ridge_a.fit(X, Y)
print('ridge_a coeffs: ', ridge_a.coef_)
ridge_b.fit(X, Y)
print('ridge_b coeffs: ', ridge_b.coef_)
lasso_a.fit(X, Y)
print('lasso_a coeffs: ', lasso_a.coef_)
lasso_b.fit(X, Y)
print('lasso_b coeffs: ', lasso_b.coef_)
print('--------------------------------------------\n')

# plot fitted Ridge curves
fig1, ax1 = plt.subplots()
ax1.set_title('Ridge Regression')
ax1.scatter(x, Y, color='black')
ax1.plot(x, linear.predict(X), '-r', label=r'$\alpha$ = 0', linewidth=2)
ax1.plot(x, ridge_a.predict(X), '-g', label=r'$\alpha$ = $10^{-5}$', linewidth=2)
ax1.plot(x, ridge_b.predict(X), '-b', label=r'$\alpha$ = 1', linewidth=2)
# add legend and curve labels
ax1.legend()
ax1.set_xlabel('x')
ax1.set_ylabel('y')

# plot fitted Lasso curves
fig2, ax2 = plt.subplots()
ax2.set_title('Lasso Regression')
ax2.scatter(x, Y, color='black')
ax2.plot(x, linear.predict(X), '-r', label=r'$\alpha$ = 0', linewidth=2)
ax2.plot(x, lasso_a.predict(X), '-g', label=r'$\alpha$ = $10^{-7}$', linewidth=2)
ax2.plot(x, lasso_b.predict(X), '-b', label=r'$\alpha$ = 1', linewidth=2)
# add legend and curve labels
ax2.legend()
ax2.set_xlabel('x')
ax2.set_ylabel('y')

# plot fitted Lasso curves
fig3, ax3 = plt.subplots()
ax3.set_title('Regression Comparison')
ax3.plot(x, linear.predict(X), '-r', label=r'Linear Regression', linewidth=2)
ax3.plot(x, ridge_a.predict(X), '-g', label=r'Ridge, $\alpha$ = $10^{-5}$', linewidth=2)
ax3.plot(x, ridge_b.predict(X), '-b', label=r'Ridge, $\alpha$ = 1', linewidth=2)
ax3.plot(x, lasso_a.predict(X), '--g', label=r'Lasso, $\alpha$ = $10^{-7}$', linewidth=2)
ax3.plot(x, lasso_b.predict(X), '--b', label=r'Lasso, $\alpha$ = 1', linewidth=2)
ax3.legend()

# define alphas with which models will be penalized
alphas = np.linspace(0.0000001, 1000, 1000)
# declare arrays that store the parameters associated with each fit
coefs_ridge = []
coefs_lasso = []
for alpha in alphas:
    # create models for current alpha
    ridge = Ridge(alpha=alpha, solver=solver)
    lasso = Lasso(alpha=alpha)
    # train models
    ridge.fit(X, Y)
    lasso.fit(X, Y)
    # store coefficients
    coefs_ridge.append(ridge.coef_)
    coefs_lasso.append(lasso.coef_)

# calculate log of alphas used
log_alphas = np.log(alphas)

# plot change in ridge coefficients as alpha increases
fig4, ax4 = plt.subplots()
for i in range(0, p, 1):
    # gather all values of beta_i across alpha
    beta_ridge = [coefs[i] for coefs in coefs_ridge]
    # plot these values
    ax4.plot(log_alphas, beta_ridge, label=r'$\beta_{{{index}}}$'.format(index=i + 1))
# format plot
ax4.set_xlabel(r'$log(\alpha)$')
ax4.set_ylabel(r'$\beta_i$')
ax4.set_title('Ridge')
fig4.legend()

# plot change in lasso coefficients as alpha increases
fig5, ax5 = plt.subplots()
for i in range(0, p, 1):
    # gather all values of beta_i across alpha
    beta_lasso = [coefs[i] for coefs in coefs_lasso]
    # plot these values
    ax5.plot(log_alphas, beta_lasso, label=r'$\beta_{{{index}}}$'.format(index=i + 1))
# format plot
ax5.set_xlabel(r'$log(\alpha)$')
ax5.set_ylabel(r'$\beta_i$')
ax5.set_title('Lasso')
ax5.legend()

plt.show()


