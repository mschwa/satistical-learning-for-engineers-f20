import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import Ridge, Lasso

# set random seed
np.random.seed(0)

# set number of samples
N = 100
solver = 'sag'    # sag or svd

# generate random X and epsilon values
X = 5 * np.random.uniform(size=N) - 2
epsilon = np.random.normal(0, 1, N)
# sort X values for plots to be generated correctly and caculate output vector
X = np.sort(X)
Y = 0.5 * X**2 + 0.5 * X + 1 + epsilon

# reshape input to 2D column array (required for fitting)
X = X.reshape(len(X), 1)

# create ridge and lasso models
ridge_0 = Ridge(alpha=0, solver=solver)
ridge_10 = Ridge(alpha=10, solver=solver)
ridge_100 = Ridge(alpha=100, solver=solver)
lasso_01 = Lasso(alpha=0.1)
lasso_1 = Lasso(alpha=1)

# train ridge regression models
ridge_0.fit(X, Y)
ridge_10.fit(X, Y)
ridge_100.fit(X, Y)
lasso_01.fit(X, Y)
lasso_1.fit(X, Y)

# plot fitted Ridge curves
fig1, ax1 = plt.subplots()
ax1.set_title('Ridge Regression')
ax1.scatter(X, Y, color='black')
ax1.plot(X, ridge_0.predict(X), '-k', label=r'$\alpha$ = 0', linewidth=2)
ax1.plot(X, ridge_10.predict(X), '-g', label=r'$\alpha$ = 10', linewidth=2)
ax1.plot(X, ridge_100.predict(X), '-b', label=r'$\alpha$ = 100', linewidth=2)
# add legend and curve labels
ax1.legend()
ax1.set_xlabel('x')
ax1.set_ylabel('y')

# plot fitted Lasso curves
fig2, ax2 = plt.subplots()
ax2.set_title('Lasso Regression')
ax2.scatter(X, Y, color='black')
ax2.plot(X, ridge_0.predict(X), '-k', label=r'$\alpha$ = 0', linewidth=2)
ax2.plot(X, lasso_01.predict(X), '-g', label=r'$\alpha$ = 0.1', linewidth=2)
ax2.plot(X, lasso_1.predict(X), '-b', label=r'$\alpha$ = 1', linewidth=2)
# add legend and curve labels
ax2.legend()
ax2.set_xlabel('x')
ax2.set_ylabel('y')

# plot fitted Lasso curves
fig3, ax3 = plt.subplots()
ax3.set_title('Regression Comparison')
ax3.plot(X, ridge_0.predict(X), '-k', label=r'Linear Regression', linewidth=2)
ax3.plot(X, ridge_10.predict(X), '-g', label=r'Ridge $\alpha$ = 10', linewidth=2)
ax3.plot(X, ridge_100.predict(X), '-b', label=r'Ridge $\alpha$ = 100', linewidth=2)
ax3.plot(X, lasso_01.predict(X), '--g', label=r'Lasso $\alpha$ = 0.1', linewidth=2)
ax3.plot(X, lasso_1.predict(X), '--b', label=r'Lasso $\alpha$ = 1', linewidth=2)
ax3.legend()

plt.show()


