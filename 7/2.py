import numpy as np
import matplotlib.pyplot as plt
from sklearn.datasets import fetch_openml
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA

# load MNIST hand-written zip codes
X, y = fetch_openml('mnist_784', return_X_y=True)

# standardize input features by removing the mean and scaling to unit variance
scaler = StandardScaler()
X_normalized = scaler.fit_transform(X)

# define number of principal components as percentage of variance preserved
n_components = 0.95
# apply PCA
pca = PCA(n_components=n_components)
X_principal = pca.fit_transform(X_normalized)

# transform data back to its original space
X_reduced = pca.inverse_transform(X_principal)

# generate cumulative explained variance vs. dimensions (number of principal
# components) plot. This curve quantifies how much of the total,
# full-dimensional variance is contained within the first N components
fig1, ax1 = plt.subplots()
ax1.plot(np.cumsum(pca.explained_variance_ratio_))
ax1.set_xlabel('Number of Components')
ax1.set_ylabel('Cumulative Explained Variance')

# plot first image in original dataset
fig2, ax2 = plt.subplots()
ax2.imshow(X[0].reshape(28, 28), cmap='gray')
ax2.set_title('Before PCA')

# plot first image in reduced dataset
fig3, ax3 = plt.subplots()
ax3.imshow(X_reduced[0].reshape(28, 28), cmap='gray')
ax3.set_title('After PCA')


plt.show()





