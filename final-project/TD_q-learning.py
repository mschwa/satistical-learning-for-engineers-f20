import gym
import math
import numpy as np
import numpy.random as rnd
from collections import defaultdict

env = gym.make("CartPole-v0")
observation = env.reset()

### Example 1

# for _ in range(1000):
#   env.render()
#   action = env.action_space.sample() # your agent here (this takes random actions)
#   observation, reward, done, info = env.step(action)

#   if done:
#     observation = env.reset()
# env.close()

# def basic_policy(obs):
#     angle = obs[2]
#     return 0 if angle < 0 else 1


### Example 2

# totals = []

# for episode in range (500):
#     episode_rewards = 0
#     obs = env.reset()
#     for step in range(1000):
#         env.render()
#         action = basic_policy(obs)
#         obs, reward, done, info = env.step(action)
#         episode_rewards += reward
#         if done: break

#     totals.append(episode_rewards)

# env.close()


### Example 3: Q-learning

# system characteristics
M    = 1       # kg
m    = 0.1     # kg
g    = -9.8    # m/s^2
l    = 0.5     # m
mu_c = 0.0005
mu_p = 0.000002
dt    = 0.02    # s


def angular_state(s, a):
    """
        Update angular state.
    """
    global g, M, m, l, mu_c, mu_p, dt
    # retrieve state components (values at t)
    theta, theta_vel, x, x_vel = s
    # calculate angular acceleration
    num = g * math.sin(theta) + math.cos(theta) \
          * (-a - m * l * theta_vel**2 * math.sin(theta) + mu_c * math.copysign(1, x_vel) / (M + m)) \
          - mu_p * theta_vel / (m * l)
    den = l * (4/3 - m * math.cos(theta)**2 / (m + M))
    theta_acc = num / den
    # update angular velocity and position with Euler's method (values at t+1)
    theta     += dt * theta_vel
    theta_vel += dt * theta_acc
    # return angular state
    return theta, theta_vel, theta_acc


def linear_state(s, a, theta_acc):
    """
        Update linear state.
    """
    global M, m, l, mu_c, dt
    # retrieve state components (values at t)
    theta, theta_vel, x, x_vel = s
    # calculate linear acceleration
    x_acc = (a + m * l * (theta_vel**2 * math.sin(theta) - theta_acc * math.cos(theta)) \
            - mu_c * math.copysign(1, x_vel)) / (M + m)
    # update linear velocity and acceleration with Euler's method (values at t+1)
    x     += dt * x_vel
    x_vel += dt * x_acc
    # return linear state
    return x, x_vel, x_acc


def update_state(s, a):
    """
        Calculate new state from current state and action.
    """
    # get new angular state
    theta, theta_vel, theta_acc = angular_state(s, a)
    # get new linear state
    x, x_vel, x_acc = linear_state(s, a, theta_acc)
    # form new state
    s_new = (theta, theta_vel, x, x_vel)
    # calculate rewards
    reward = +1 if (math.abs(theta) <= 12 or math.abs(x) <= 2.4) else 0
    # return state
    return reward, s_new


def discretize_state(s):
    """
        Convert continuous state values into discrete states.
    """
    # retrieve state variables
    theta, theta_vel, x, x_vel = s
    # discretize state variables
    theta_idx     = np.digitize(theta, theta_bins)
    theta_vel_idx = np.digitize(theta_vel, theta_vel_bins)
    x_idx         = np.digitize(x, x_bins)
    x_vel_idx     = np.digitize(x, x_vel_bins)
    # return discretized state
    return (theta_idx, theta_vel_idx, x_idx, x_vel_idx)


def q_learn(s, a, r, s_new, lr):
    """
        Update record of q-values.
    """
    global Q, discount, possible_actions
    # discretize states
    s_discrete     = discretize_state(s)
    s_new_discrete = discretize_state(s_new)
    # form current q-state
    q_state = s_discrete + (a,)
    # calculate maximum q-value over actions taken in next state
    max_q_new = 0
    for action in possible_actions:
        q = Q[s_new_discrete + (action,)]
        if (q > max_q_new): max_q_new = q
    # update q-value
    Q[q_state] += lr * (r + discount * max_q_new - Q[q_state])

# create discretized, non-terminal state bins
theta_bins     = np.array([math.radians(x) for x in [-12, -6, -1, 0, 1, 6, 12]])
x_bins         = np.array([-2.4, -0.8, 0.8, 2.4])
theta_vel_bins = np.array([-np.inf, -math.radians(50), -math.radians(50), np.inf])
x_vel_bins     = np.array([-np.inf, -0.5, 0.5, np.inf])
# list possible actions
possible_actions = [-10, 10]
# define learning parameters
lr_0         = 0.05
lr_decay     = 0.1
n_iterations = 20000
discount     = 0.9

# declare initial state
s = (0, 0, 0, 0)
# declare table mapping state to q-value. Default value state is zero
Q = defaultdict(lambda: 0)

for iteration in range(n_iterations):
    # randomly choose action
    a = rnd.choice(possible_actions)
    # calculate next state using model
    r, s_new = update_state(s ,a)
    # check whether agent failed: if reward is zero, balance was lost
    if (r == 0): break
    # update learning rate
    lr = lr_0 / (1 + iteration * lr_decay)
    q_learn(s, a, r, s_new, lr)
    # update current state
    s = s_new



