"""
References:
    - https://www.vebuso.com/2020/03/svm-hyperparameter-tuning-using-gridsearchcv/
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap

# sklearn imports
import sklearn.neighbors as nb
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.preprocessing import OneHotEncoder, LabelEncoder, StandardScaler
from sklearn.impute import SimpleImputer
from sklearn.svm import SVC

# declare encoders
OH_encoder = OneHotEncoder(handle_unknown='ignore', sparse=False)
label_encoder = LabelEncoder()

# variable to hold dimension of input
dim = None

def preprocess(haptitis_data):
    # separate target from features and label it
    y = haptitis_data['target']
    y = label_encoder.fit_transform(y)
    X = haptitis_data.drop(['target'], axis=1)

    # drop ID column since it contains unique values and adds no information
    # to the model
    X = X.drop(['ID'], axis=1)

    # columns with categorical values to be one-hot encoded (no hierarchy
    # among column values)
    one_hot_cols = ['gender', 'steroid', 'antivirals', 'fatigue', 'malaise',
                    'anorexia', 'liverBig', 'liverFirm', 'spleen', 'spiders',
                    'ascites', 'varices', 'histology']

    # one-hot encode appropriate columns
    X_encoded = X[one_hot_cols].copy()
    # save indexes to put the back on frame (one-hot encoding drops them)
    index = X_encoded.index
    # encode
    X_encoded = pd.DataFrame(OH_encoder.fit_transform(X_encoded))
    # reassign index
    X_encoded.index = index

    # inspection of data shows that only lead_time column is the only one
    # containing null values. Replace those with the mean value for the
    # column
    imputer = SimpleImputer()
    X_imputed = pd.DataFrame(imputer.fit_transform(X_encoded))
    # imputation removes column names, so put them back
    X_imputed.columns = X_encoded.columns
    
    # standardize input features by removing the mean and scaling to unit variance
    scaler = StandardScaler()
    X_normalized = scaler.fit_transform(X_imputed)

    # set number of features in data point
    dim = X_normalized.shape[1]

    # return train and test splits
    return train_test_split(X_normalized, y, train_size=0.9, test_size=0.1, random_state=0)



def train_and_test_default(X_train, X_test, y_train, y_test):
    svc = SVC()
    # train default model
    svc.fit(X_train, y_train)
    # get mean accuracy of model with test data
    score = svc.score(X_test, y_test)
    print('Default SVC accuracy: {}'.format(score))


def train_and_test_grid_search(X_train, X_test, y_train, y_test):
    # define parameters to search for along with their ranges. C is the slack
    # in SVMs and serves as a regularization paramter. Gamma is the kernel
    # coefficient for the kernels chosen below. For more details, see:
    # https://scikit-learn.org/stable/auto_examples/svm/plot_rbf_parameters.html
    param_grid = {
        'C': [0.1, 1, 10, 100],
        'gamma': [1, 0.1, 0.01, 0.001],
        'kernel': ['rbf', 'poly', 'sigmoid']
    }
    # create grid search and fit for parameter search
    result = GridSearchCV(SVC(), param_grid, refit=True)
    result.fit(X_train, y_train)
    # print best model's parameters
    print(result.best_estimator_)
    # evaluate mean accuraccy
    score = result.score(X_test, y_test)
    print('Tuned SVC accuracy: {}'.format(score))


if __name__ == '__main__':
    # load content of csv file into DataFrame object
    data = pd.read_csv('./data/hepatitis.csv', sep=',')
    # preprocess data and get set splits
    X_train, X_test, y_train, y_test = preprocess(data)
    # compare results without and with cross validation
    train_and_test_default(X_train, X_test, y_train, y_test)
    train_and_test_grid_search(X_train, X_test, y_train, y_test)
