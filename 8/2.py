import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap

# sklearn imports
import sklearn.neighbors as nb
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import OneHotEncoder, LabelEncoder, StandardScaler
from sklearn.impute import SimpleImputer

# keras imports
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras.regularizers import l2

# declare encoders
OH_encoder = OneHotEncoder(handle_unknown='ignore', sparse=False)
label_encoder = LabelEncoder()

# variable to hold dimension of input
p = None

def preprocess(backorder_data):
    global p

    # separate target from features and label it
    y = backorder_data['went_on_backorder']
    y = label_encoder.fit_transform(y)
    X = backorder_data.drop(['went_on_backorder'], axis=1)

    # drop SKU column since it contains unique values and adds no information
    # to the model
    X = X.drop(['sku'], axis=1)

    # columns with categorical values to be one-hot encoded (no hierarchy
    # among column values)
    one_hot_cols = ['potential_issue', 'deck_risk', 'oe_constraint', 'ppap_risk',
                    'stop_auto_buy', 'rev_stop']

    # one-hot encode appropriate columns
    X_encoded = X[one_hot_cols].copy()
    # save indexes to put the back on frame (one-hot encoding drops them)
    index = X_encoded.index
    # encode
    X_encoded = pd.DataFrame(OH_encoder.fit_transform(X_encoded))
    # reassign index
    X_encoded.index = index

    # inspection of data shows that only lead_time column is the only one
    # containing null values. Replace those with the mean value for the
    # column
    imputer = SimpleImputer()
    X_imputed = pd.DataFrame(imputer.fit_transform(X_encoded))
    # imputation removes column names, so put them back
    X_imputed.columns = X_encoded.columns
    
    # standardize input features by removing the mean and scaling to unit variance
    scaler = StandardScaler()
    X_normalized = scaler.fit_transform(X_imputed)

    # set number of features in data point
    p = X_normalized.shape[1]

    # return train and test splits
    return train_test_split(X_normalized, y, train_size=0.9, test_size=0.1, random_state=0)


def build_models():
    # model 1
    model_1 = Sequential()
    model_1.add(Dense(1, input_shape=(p,), activation='sigmoid'))
    model_1.compile(loss='binary_crossentropy',
                    optimizer='sgd',
                    metrics=['accuracy'])
    print(model_1.summary())
    
    # model 2
    model_2 = Sequential()
    model_2.add(Dense(15, input_dim=p, activation='tanh'))
    model_2.add(Dense(1, activation='sigmoid'))
    model_2.compile(loss='binary_crossentropy',
                    optimizer='sgd',
                    metrics=['accuracy'])
    print(model_2.summary())
    
    # model 3
    model_3 = Sequential()
    model_3.add(Dense(25, input_dim=p, activation='tanh'))
    model_3.add(Dense(15, activation='tanh'))
    model_3.add(Dense(1, activation='sigmoid'))
    model_3.compile(loss='binary_crossentropy',
                    optimizer='sgd',
                    metrics=['accuracy'])
    print(model_3.summary())
    
    # model 4
    # decay = 1e-5
    model_4 = Sequential()
    model_4.add(Dense(25, input_dim=p, activation='tanh', kernel_regularizer='l2'))
    model_4.add(Dense(15, activation='tanh', kernel_regularizer='l2'))
    model_4.add(Dense(1, activation='sigmoid'))
    model_4.compile(loss='binary_crossentropy',
                    optimizer='sgd',
                    metrics=['accuracy'])
    print(model_4.summary())
    
    return [model_1, model_2, model_3, model_4]


def train_models(models, X_train, y_train):
    for i, model in enumerate(models):
        # train model and retrieve statics history
        history = model.fit(X_train,
                            y_train,
                            validation_split=0.2,
                            epochs=100)
        # create figure with two subplots
        fig, (ax_left, ax_right) = plt.subplots(nrows=1, ncols=2)
        # plot accuracy history for train and validation sets
        ax_left.plot(history.history['acc'], label='Train')
        ax_left.plot(history.history['val_acc'], label='Validation')
        ax_left.title.set_text('Model {} - Accuracy'.format(i + 1))
        ax_left.set_xlabel('Epoch')
        ax_left.set_ylabel('Accuracy')
        ax_left.legend()
        # plot loss history for train and validation sets
        ax_right.plot(history.history['loss'], label='Train')
        ax_right.plot(history.history['val_loss'], label='Validation')
        ax_right.title.set_text('Model {} - Loss'.format(i + 1))
        ax_right.set_xlabel('Epoch')
        ax_right.set_ylabel('Loss')
        ax_right.legend()

    return models


def test_models(models, X_test, y_test):
    for i, model in enumerate(models):
        score, acc = model.evaluate(X_test, y_test)
        print('Model {} accuracy: {}'.format(i + 1, acc))


if __name__ == '__main__':
    # load content of csv file into DataFrame object
    data = pd.read_csv('./data/BackOrders.csv', sep=',')
    # preprocess data and get set splits
    X_train, X_test, y_train, y_test = preprocess(data)
    # build models
    models = build_models()
    # train models
    models = train_models(models, X_train, y_train)
    # test models
    test_models(models, X_test, y_test)
    # display plots
    plt.show()
