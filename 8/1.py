import math
import numpy as np

# define sigmoid function
sigmoid = lambda x: 1 / (1 + math.exp(-x))

# derivative of sigmoid function
def sigmoid_prime(x):
    sig = sigmoid(x)
    return sig * (1 - sig)


def feed_forward(x):
    # calculate output of each hidden node
    Z_1 = sigmoid(np.dot(x, a1))
    Z_2 = sigmoid(np.dot(x, a2))
    # combine outputs into array
    Z = np.array([Z_1, Z_2])
    # calculate network output
    out = np.dot(Z, b)
    return Z, out


### Back-propagation equations #########################################

def delta(y, y_hat):
    """
        aj represents the weights (pointing) to jth hidden node
    """
    # calculate output of each neuron in hidden layer
    z_1 = sigmoid(np.dot(a1, x))
    z_2 = sigmoid(np.dot(a2, x))
    # combine individual outputs to give vector output of hidden layer
    z = np.array([z_1, z_2])
    # calculate delta
    delta = 2 * (y - y_hat) * np.dot(b, z)
    return z, delta


def e(delta, j, a, x):
    """
        - aj represents the weights (pointing) to jth hidden node
        - b represents the weights (pointing) to the single output node
        - bj represents the weight from the jth hidden node to the output node
    """
    return delta * b[j] * sigmoid_prime(np.dot(a, x))


def back_propagate(y, y_hat, x):
    # calculate delta_ki values (11.14)
    Z, d = delta(y, y_hat)
    # calculate new components of a1
    a1_new = np.zeros(2)
    a1_new[0] = a1[0] + lr * e(d, 0, a1, x) * x[0]
    a1_new[1] = a1[1] + lr * e(d, 1, a1, x) * x[1]
    # calculate new components of a2
    a2_new = np.zeros(2)
    a2_new[0] = a2[0] + lr * e(d, 0, a2, x) * x[0]
    a2_new[1] = a2[1] + lr * e(d, 1, a2, x) * x[1]
    # calculate new components of b
    b_new = np.zeros(2)
    b_new[0] = b[0] + lr * d * Z[0]
    b_new[1] = b[1] + lr * d * Z[1]
    # return updated weights
    return a1_new, a2_new, b_new

###############################################################################

# define learning rate
lr = 1
# initialize weights (as weights pointing to given node) as glonal variables
a1 = np.array([0.1, 0.3])
a2 = np.array([0.3, 0.4])
b = np.array([0.4, 0.6])

if __name__ == '__main__':
    # initialize training sample
    x = np.array([0, 1])
    y = 1
    # keep previous prediction
    y_hat_prev = float('inf')
    while(True):
        z, y_hat = feed_forward(x)
        print('y_hat: {}'.format(y_hat))
        # terminate training?
        if (abs(y_hat - y_hat_prev) < 1/1e6):
            break
        else:
            y_hat_prev = y_hat
        # back-propagate and update weights
        a1, a2, b = back_propagate(y, y_hat, x)